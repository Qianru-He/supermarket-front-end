const initState = {
    orders: []
};

export default (states = initState, action) => {
    if (action.type === 'GET_ORDERS') {
        return {
            ...states,
            orders: action.orders
        };
    } else {
        return states;
    }
}