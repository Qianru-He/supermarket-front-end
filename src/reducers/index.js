import {combineReducers} from "redux";
import productsReducer from "./productsReducer";
import ordersReducer from "./ordersReducer";

const reducers = combineReducers({
    productsReducer,
    ordersReducer
});
export default reducers;