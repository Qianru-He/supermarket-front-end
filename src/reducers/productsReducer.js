const initState = {
    products: []
};

export default (states = initState, action) => {
    if (action.type === 'GET_PRODUCTS') {
        return {
            ...states,
            products: action.products
        };
    } else {
        return states;
    }
}