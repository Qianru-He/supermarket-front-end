import React, {Component} from 'react';
import {MdAddCircleOutline} from "react-icons/md";
import './ProductDetail.less'
import {connect} from "react-redux";
import {addOrders} from "../../action/orderAction";

class ProductDetail extends Component {
    constructor(props){
        super(props)
        this.handleOnClick = this.handleOnClick.bind(this);
    }
    render() {
        console.log(this.props.image)
        return (
            <li className={'productDetail'}>
                <img src={this.props.image}/>
                <div className={'productInfo'}>
                    <h3>{this.props.name}</h3>
                    <div>单价：{this.props.price}元/{this.props.unit}</div>
                </div>
                <div>
                    <button className={'add-product'} onClick={()=>this.handleOnClick(this.props.id)}><MdAddCircleOutline/></button>
                </div>
            </li>
        );
    }

    handleOnClick(id) {
        this.props.addOrders(id);
    }
}
const mapDispatchToProps = (dispatch) => ({
    addOrders: (id) => dispatch(addOrders(id))
});
export default connect(null, mapDispatchToProps)(ProductDetail);