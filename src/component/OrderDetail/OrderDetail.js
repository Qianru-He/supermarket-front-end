import React, {Component} from 'react';
import './OrderDetail.less'
import {connect} from "react-redux";
import {deleteOrderById} from "../../action/orderAction";

class OrderDetail extends Component {
    constructor(props) {
        super(props)
        this.handleOnclick = this.handleOnclick.bind(this);
    }

    render() {
        console.log(this.props);
        return (
            <li>
                <span className={'order-line'}>{this.props.name}</span>
                <span className={'order-line'}>{this.props.price}</span>
                <span className={'order-line'}>{this.props.numbers}</span>
                <span className={'order-line'}>{this.props.unit}</span>
                <button className={'delete-button'} onClick={() => this.handleOnclick(this.props.id)}>删除</button>
            </li>
        );
    }

    handleOnclick(id) {
        console.log(id);
        this.props.deleteOrderById(id);
    }
}

const mapDispatchToProps = (dispatch) => ({
    deleteOrderById: (id) => dispatch(deleteOrderById(id))
});
export default connect(null, mapDispatchToProps)(OrderDetail);