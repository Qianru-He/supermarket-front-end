import React, {Component} from 'react';
import {connect} from "react-redux";
import {getOrders} from "../../action/orderAction";
import OrderDetail from "../OrderDetail/OrderDetail";
import './DisplayOrders.less'

class DisplayOrders extends Component {
    componentDidMount() {
        this.props.getOrders();
    }

    render() {
        const {orders} = this.props;
        console.log(this.props);
        console.log(orders);
        return (
            <div>
                <li className={'orders'}>
                    <span className={'order-line'}>名字</span>
                    <span className={'order-line'}>单价</span>
                    <span className={'order-line'}>数量</span>
                    <span className={'order-line'}>单位</span>
                    <span className={'order-line'}>操作</span>
                </li>
                {
                    orders.map((item, index) => {
                        return <OrderDetail key={index} id={item.id} name={item.product.name} price={item.product.price}
                                            numbers={item.numbers} unit={item.product.unit}/>
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    orders: state.ordersReducer.orders
});
const mapDispatchToProps = (dispatch) => ({
    getOrders: (order) => dispatch(getOrders(order))
});
export default connect(mapStateToProps, mapDispatchToProps)(DisplayOrders);