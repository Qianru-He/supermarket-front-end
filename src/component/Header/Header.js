import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import {MdAdd, MdAddShoppingCart, MdHome} from "react-icons/md";
import './Header.less'

class Header extends Component {
    render() {
        return (
            <header className='header'>
                <NavLink exact className='link' activeClassName="active" to='/'><MdHome/>商城</NavLink>
                <NavLink className='link' activeClassName="active" to='/orders'><MdAddShoppingCart/>订单</NavLink>
                <NavLink className='link' activeClassName="active" to='/create'><MdAdd/>添加商品</NavLink>
            </header>
        );
    }
}

export default Header;