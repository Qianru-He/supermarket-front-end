import React, {Component} from 'react';
import {getProducts} from "../../action/productAction";
import {connect} from "react-redux";
import ProductDetail from "../ProductDetail/ProductDetail";

class DisplayProducts extends Component {
    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        console.log(this.props)
        const {products} = this.props;
        return (
            <div>
                <ul>
                    {
                        products.map((product, index) => {
                            console.log(product.name);
                            return <ProductDetail key={index} id={product.id} name={product.name} price={product.price}
                                                  unit={product.unit} image={product.image}/>
                        })
                    }
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    products: state.productsReducer.products
});
const mapDispatchToProps = (dispatch) => ({
    getProducts: (product) => dispatch(getProducts(product))
});
export default connect(mapStateToProps, mapDispatchToProps)(DisplayProducts);