import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import './CreateProduct.less'
import {postProduct} from "../../action/productAction";
import {connect} from "react-redux";

class CreateProduct extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            name:'',
            price:'',
            unit:'',
            image:''
        };
        this.handleProductName=this.handleProductName.bind(this);
        this.handleProductPrice=this.handleProductPrice.bind(this);
        this.handleProductUnit=this.handleProductUnit.bind(this);
        this.handleProductImage=this.handleProductImage.bind(this);
        this.handlePostProduct=this.handlePostProduct.bind(this);
    }

    render() {
        return (
            <div>
                <section className={'create-product'}>
                    <h1>添加商品</h1>
                    <div>
                        <label>名称</label><br/>
                        <input className={'content'} value={this.state.name} onChange={this.handleProductName} type="text"/>
                    </div>
                    <div>
                        <label>价格</label><br/>
                        <input className={'content'} value={this.state.price} onChange={this.handleProductPrice} type="number"/>
                    </div>
                    <div>
                        <label>单位</label><br/>
                        <input className={'content'} value={this.state.unit} onChange={this.handleProductUnit} type="text"/>
                    </div>
                    <div>
                        <label>图片</label><br/>
                        <input className={'content'} value={this.state.image} onChange={this.handleProductImage} type="text"/>
                    </div>
                    <div>
                        <NavLink to='/'>
                            <button className={'post-product'} onClick={this.handlePostProduct}>提交</button>
                        </NavLink>
                    </div>
                </section>
            </div>
        );
    }

    handleProductName(event) {
        this.setState({name: event.currentTarget.value})
    }

    handleProductPrice(event) {
        this.setState({price: event.currentTarget.value})
    }

    handleProductUnit(event) {
        this.setState({unit: event.currentTarget.value})
    }

    handleProductImage(event) {
        this.setState({image: event.currentTarget.value})
    }

    handlePostProduct() {
        const {name, price, unit, image} = this.state;
        const product = {name, price, unit, image};
        console.log(product);
        this.props.postProduct(product);
    }
}
const mapDispatchToProps = (dispatch) => ({
    postProduct: (product) => dispatch(postProduct(product))
});

export default connect(null,mapDispatchToProps)(CreateProduct);