import React, {Component} from 'react';
import './App.less';
import Header from "./component/Header/Header";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import CreateProduct from "./component/CreateProduct/CreateProduct";
import DisplayProducts from "./component/DisplayProducts/DisplayProducts";
import DisplayOrders from "./component/DisplayOrders/DisplayOrders";

class App extends Component{
  render() {
    return (
        <Router>
          <Header/>
            <Switch>
                <Route exact path='/' component={DisplayProducts}/>
                <Route exact path='/create' component={CreateProduct}/>
                <Route path='/orders' component={DisplayOrders}/>
            </Switch>
        </Router>
    );
  }
}

export default App;