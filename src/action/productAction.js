export const postProduct = (product) => (dispatch) => {
    fetch('http://localhost:8080/api/products', {
        method: 'POST',
        body: JSON.stringify(product),
        headers: {
            'content-type': 'application/json'
        }
    })
};
export const getProducts = () => (dispatch) => {
    fetch('http://localhost:8080/api/products')
        .then(response => response.json())
        .then(products => {
                dispatch({
                    type: 'GET_PRODUCTS',
                    products: products
                })
            }
        )
};
