export const addOrders = (id) => () => {
    fetch(`http://localhost:8080/api/orders/${id}`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        }
    }).then()
};
export const getOrders = () => (dispatch) => {
    fetch('http://localhost:8080/api/orders')
        .then(response => response.json())
        .then(orders => {
                dispatch({
                    type: 'GET_ORDERS',
                    orders: orders
                })
            }
        )
};
export const deleteOrderById = (id) => (dispatch) => {
    console.log(id);
    fetch(`http://localhost:8080/api/orders/${id}`,{
        method: 'DELETE',
        headers:{
            'content-type': 'application/json'
        }
    }).then(()=>{
        dispatch(
            getOrders()
        );
    })
};